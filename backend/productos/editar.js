var user_id,nombre,descripcion,precio,stock,foto,id_editar, Respaldo_img, imgDropzone;
$(function() {
    var input = document.getElementById("iStock");
    input.onkeydown = function(e) {
        if (!((e.keyCode > 95 && e.keyCode < 106)
            || (e.keyCode > 47 && e.keyCode < 58)
            || e.keyCode === 8)) {
            return false;
        }
    };

    var input2 = document.getElementById("iPrecio");
    input2.onkeydown = function(e2) {
        if (!((e2.keyCode > 95 && e2.keyCode < 106)
            || (e2.keyCode > 47 && e2.keyCode < 58)
            || e2.keyCode === 8)) {
            return false;
        }
    };

    getId();
    // Ocultamos el Dropzone
    document.getElementById('iDropzone').style.display = 'none';
    document.getElementById('return').style.display = 'none';


});

/*$(function () {
    //getData();
    getId();
    // Ocultamos el Dropzone
    document.getElementById('iDropzone').style.display = 'none';
    document.getElementById('return').style.display = 'none';
});*/

Dropzone.options.myAwesomeDropzone = {
    paramName: "file", // The name that will be used to transfer the file
    maxFilesize: 5, // MB
    // maxFiles: 1, //Cantidad de archivos
    accept: function(file, done) {
        console.log(file.name);
        imgDropzone = 'http://localhost/webservice_Examen3pw/uploads/'+file.name;
        console.log(imgDropzone);
        done();
    }
};

function getId() {
    user_id = localStorage.getItem('userId');
    var jsData = { id: user_id };
    //alert(user_id);
    $.ajax({
        // url: 'http://localhost/webservices_Examen1/INgetDataId.php',
        url: 'http://localhost/webservice_Examen3pw/productos.php?opcion=getDataId',
        type: 'POST',
        data: JSON.stringify(jsData),

        //result es mi data en NG(Angular)
        success: function (result) {
            console.log(result);
            //Asignacion a una vareable

            document.getElementById('iNombre').value = result[0].nombre;
            document.getElementById('iDescripcion').value = result[0].descripcion;
            document.getElementById('iPrecio').value = result[0].precio;
            document.getElementById('iStock').value = result[0].stock;
            document.getElementById('iFoto').src = result[0].imagen;
            Respaldo_img = result[0].imagen;
        },
        error: function (error) {
            console.log(error);
        }
    })

}

function editar(){
    nombre = document.getElementById('iNombre').value;
    descripcion = document.getElementById('iDescripcion').value;
    precio = document.getElementById('iPrecio').value;
    stock = document.getElementById('iStock').value;
    id_editar = localStorage.getItem('userId');
    //foto = document.getElementById('iFoto').src;
    if (document.getElementById('iFoto').src == 'http://localhost/examen_2doParceal/backend/productos/NULL'){
        foto = imgDropzone
    }else {
        foto = document.getElementById('iFoto').src
    }
    // http://localhost/examen_2doParceal/backend/productos/NULL

    //console.log(nombre,correo,telefono, id_editar);

    var jsData = { nombre: nombre, descripcion: descripcion, precio: precio,stock: stock,foto: foto, id: id_editar};
    console.log(jsData);

    $.ajax({
        url: 'http://localhost/webservice_Examen3pw/productos.php?opcion=update',
        type: 'POST',
        data: JSON.stringify(jsData),

        success: function (result) {
            //console.log(result);
            //alert('Datos Actualizados Correctamente');
            Swal.fire({
                position: 'top-end',
                type: 'success',
                title: 'Actualizado correctamente',
                showConfirmButton: false,
                timer: 1500
            })
            setTimeout(function(){ window.open('ver.html','_self'); }, 1502);

            //window.open('editar.html','_self');
        },
        error: function (error) {
            console.log(error);
            //alert('Error a chuchita le bolsearon el update');
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Error a chuchita le bolsearon el update!'
            })
        }
    })
}

function ocultar(){
    document.getElementById('iImagen').style.display = 'none';
    document.getElementById('cambio').style.display = 'none';
    document.getElementById('iFoto').src = 'NULL';
    document.getElementById('iDropzone').style.display = 'inline';
    document.getElementById('return').style.display = 'inline';
}

function desocultar(){
    document.getElementById('iImagen').style.display = 'inline';
    document.getElementById('cambio').style.display = 'inline';
    document.getElementById('iDropzone').style.display = 'none';
    document.getElementById('iFoto').src = Respaldo_img;
    document.getElementById('return').style.display = 'none';
}
