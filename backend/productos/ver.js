$(document).ready(function() {
  //Configuracion de la tabla
  var table = $("#table_id").DataTable({
    dom: 'Bfrtip',
    buttons: [
      'copy', 'csv', 'excel', 'pdf',{
        extend: 'print',
        messageTop: 'Custom Message',
        title: 'Custom Title',//notExport
        className: 'btn btn-outline-primary',
        exportOptions:{
          columns: ':not(.notExport)'
        }
      }
    ],
    ajax: {
      method: "POST",
      url: "http://localhost/webservice_Examen3pw/productos.php?opcion=index"
      //url: "http://localhost/webservices_Examen1/INgetData.php"
    },
    columns: [
      { data: "id" },
      { data: "nombre" },
      { data: "descripcion" },
      { data: "precio" },
      { data: "stock" },
      { data: "imagen" },
      {
        defaultContent: '<button type="button" class="editar btn btn-outline-primary"><i class="material-icons">edit</i></button>&nbsp<button id="eli" type="button" class="borrar btn btn-outline-danger"><i class="material-icons">delete</i></button>'
      }
    ]
  });
  $("#table_id tbody").on("click", "button", function() {
    var action = this.className;
    var row = table.row( $(this).parents('tr') );

    var data = row.data();
    //var data = table.row($(this).parents("tr")).data();
    if (action=='editar btn btn-outline-primary') {
      //alert('Este es el de editar: ' + data['id']);
      editar(data['id']);
    }
    if (action=='borrar btn btn-outline-danger') {
      eliminar(data['id'],row);
    }
  });

});
//webservices_PAGWEB2

function eliminar(id,row) {
  //alert("id a eliminar: " + id);
  var jsData = { id: id };
  Swal.fire({
    title: 'Estas seguro?',
    text: "No podras revertir esto!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si borralo!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        url: 'http://localhost/webservice_Examen3pw/productos.php?opcion=delete',
        type: 'POST',
        data: JSON.stringify(jsData),

        success: function (result) {
          console.log(result);
          //alert('Se borro Correctamente');
          Swal.fire({
            position: 'top-end',
            type: 'success',
            title: 'Ya se borro',
            showConfirmButton: false,
            timer: 1500
          });
          row.remove().draw();
          //setTimeout(function(){ location.reload(); }, 1502);
//      location.reload();
        },
        error: function (error) {
          console.log(error);
          alert('Error a chuchita le bolsearon el delete');
        }
      })
    }
  })
//    console.log(JSON.parse(jsData));

}

function editar(id) {
  //alert("id a editar: " + id);
  localStorage.setItem('userId',id);
  window.open('editar.html','_self');
}

function insertar() {
  window.open('insertar.html','_self');
}
