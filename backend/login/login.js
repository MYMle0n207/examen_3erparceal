var dataData;
$(function () {
    getData();
});
function getData() {
    $.ajax({
        url: 'http://localhost/webservices_Examen1/getData.php',
        type: 'GET',
        //result es mi data en NG(Angular)
        success: function (result) {
            dataData = result;
            console.log(dataData);
        },
        error: function (error) {
            console.log(error);
        }
    })
}

function login() {
    var correo = document.getElementById('iCorreo').value;
    var password = document.getElementById('iPassword').value;
    console.log(correo);
    console.log(password);
    for(i=0;i<dataData.length;i++){
         if (dataData[i].correo==correo && dataData[i].password==password ){
             localStorage.setItem('idUser',dataData[i].id);
             Swal.fire({
                 type: 'success',
                 title: 'Entrando',
                 showConfirmButton: true,
                 timer: 1500
             })
             window.open('verDatos.html','_self');
             break;
         }else{
             Swal.fire({
                 type: 'error',
                 title: 'Oops...',
                 text: 'Correo o Contraseña incorrectos!'
             })
         }
    }
}
