var user_id,nombre,descripcion,precio,stock,foto,id_editar, Respaldo_img, imgDropzone;

$(document).ready(function() {
    var input = document.getElementById("iStock");
    input.onkeydown = function(e) {
        if (!((e.keyCode > 95 && e.keyCode < 106)
            || (e.keyCode > 47 && e.keyCode < 58)
            || e.keyCode === 8)) {
            return false;
        }
    }

    var input2 = document.getElementById("iPrecio");
    input2.onkeydown = function(e2) {
        if (!((e2.keyCode > 95 && e2.keyCode < 106)
            || (e2.keyCode > 47 && e2.keyCode < 58)
            || e2.keyCode === 8)) {
            return false;
        }
    }


    getId();
});
/*$(function () {
    //getData();
    getId();

});*/

function getId() {
    user_id = localStorage.getItem('articuloId');
    var jsData = { id: user_id };
    //alert(user_id);
    $.ajax({
        //Codigo Prron
        // url: 'http://localhost/webservices_Examen1/INgetDataId.php',
        url: 'http://localhost/webservice_Examen3pw/productos.php?opcion=getDataId',
        type: 'POST',
        data: JSON.stringify(jsData),

        //result es mi data en NG(Angular)
        success: function (result) {
            console.log(result);
            //Asignacion a una vareable

            nombre = document.getElementById('iNombre').value = result[0].nombre;
            descripcion = document.getElementById('iDescripcion').value = result[0].descripcion;
           precio = document.getElementById('iPrecio').value = result[0].precio;
            document.getElementById('iStock').value = result[0].stock;
            stock = document.getElementById('iStock').value = result[0].stock;
            foto = document.getElementById('iFoto').src = result[0].imagen;
            Respaldo_img = result[0].imagen;
            if (stock == '0'){
                document.getElementById('Comprar').style.display = 'none';
            }
        },
        error: function (error) {
            console.log(error);
        }
    })

}

function editar(){
    nombre = document.getElementById('iNombre').value;
    descripcion = document.getElementById('iDescripcion').value;
    precio = document.getElementById('iPrecio').value;
    stock = document.getElementById('iStock').value;
    id_editar = localStorage.getItem('articuloId');
    foto = document.getElementById('iFoto').src;
    // http://localhost/examen_2doParceal/backend/productos/NULL

    //console.log(nombre,correo,telefono, id_editar);

    var jsData = { nombre: nombre, descripcion: descripcion, precio: precio,stock: stock,foto: foto, id: id_editar};
    console.log(jsData);

    $.ajax({
        url: 'http://localhost/webservice_Examen3pw/productos.php?opcion=update',
        type: 'POST',
        data: JSON.stringify(jsData),

        success: function (result) {
            //console.log(result);
            //alert('Datos Actualizados Correctamente');
            Swal.fire({
                position: 'top-end',
                type: 'success',
                title: 'Actualizado correctamente',
                showConfirmButton: false,
                timer: 1500
            })
            setTimeout(function(){ window.open('ver.html','_self'); }, 1502);

            //window.open('editar.html','_self');
        },
        error: function (error) {
            console.log(error);
            //alert('Error a chuchita le bolsearon el update');
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Error a chuchita le bolsearon el update!'
            })
        }
    })
}

function Compra() {
    Swal.fire({
        title: 'Que cantidad deseas comprar?',
        type: 'question',
        input: 'range',
        inputAttributes: {
            min: 1,
            max: stock,
            step: 1
        },
        inputValue: 1
    }).then((result) => {
        console.log(result.value);
        // Empieza el proceso de compra
        // `id`, `nombre`, `cantidad`, `imagen`, `total`, `descripcion`, `precio`
        id_editar = localStorage.getItem('articuloId');
        var jsData = {
            cantidad: result.value,
            stockAntes: stock,
            nombre: nombre,
            imagen: foto,
            descripcion: descripcion,
            precio: precio,
            id: id_editar
        };
        console.log(jsData);
        $.ajax({
            url: 'http://localhost/webservice_Examen3pw/carrito.php?opcion=create',
            type: 'POST',
            data: JSON.stringify(jsData),

            success: function (result) {
                console.log(result);
                //alert('Datos Actualizados Correctamente');
                Swal.fire({
                    position: 'top-end',
                    type: 'success',
                    title: 'Agregado al carrito correctamente, Regresando al catalogo',
                    showConfirmButton: false,
                    timer: 1500
                })
                setTimeout(function () {
                    window.open('ver.html', '_self');
                }, 1502);

                //window.open('editar.html','_self');
            },
            error: function (error) {
                console.log(error);
                //alert('Error a chuchita le bolsearon el update');
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Error a chuchita le bolsearon el update!'
                })
            }
        })
        //Termina
    })
}

function Return() {
    window.open('ver.html','_self');
}
