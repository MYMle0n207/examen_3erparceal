var Total, QueryRepuesto;
$(document).ready(function() {
    //QueryRepuesto
    sacarTotal();
    // Guardamos el Query del carrito para regresar el stock del producto
    $.ajax({
        // url: 'http://localhost/webservices_Examen1/INgetDataId.php',
        url: 'http://localhost/webservice_Examen3pw/carrito.php?opcion=indexStock',
        type: 'POST',
        data: JSON.stringify('{"Hola":1}'),

        //result es mi data en NG(Angular)
        success: function (result) {
            console.log(result);
            QueryRepuesto = result;
            console.log(QueryRepuesto);
        },
        error: function (error) {
            console.log(error);
        }
    })
    //Configuracion de la tabla
    var table = $("#table_id").DataTable({
        // "destroy": true,
        // "processing": true,
        "processing": true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf',{
                extend: 'print',
                messageTop: 'Custom Message',
                title: 'Custom Title',//notExport
                className: 'btn btn-outline-primary',
                exportOptions:{
                    columns: ':not(.notExport)'
                }
            }
        ],
        ajax: {
            method: "POST",
            url: "http://localhost/webservice_Examen3pw/carrito.php?opcion=index"
            //url: "http://localhost/webservices_Examen1/INgetData.php"
        },
        columns: [
            /*
           <th>id</th>
                <th>Nombre</th>
                <th>Descripcion</th>
                <th>Precio</th>
                <th>Cantidad</th>
                <th>Total</th>
                <th>URL Imagen</th>
             */
            { data: "id" },
            { data: "nombre" },
            { data: "descripcion" },
            { data: "precio" },
            { data: "cantidad" },
            { data: "total" },
            {
                data: "imagen",
                "render" : function (data) {
                    return '<img src="'+data+'" style="width: 150px"/>';
                }
            }/*,
            {
                defaultContent: '<button type="button" class="editar btn btn-outline-primary"><i class="material-icons">edit</i></button>'
            }*/
        ]
    });
    $("#table_id tbody").on("click", "button", function() {
        var action = this.className;
        var row = table.row( $(this).parents('tr') );

        var data = row.data();
        //var data = table.row($(this).parents("tr")).data();
        if (action=='editar btn btn-outline-primary') {
            //alert('Este es el de editar: ' + data['id']);
            editar(data['id']);
        }
        if (action=='borrar btn btn-outline-danger') {
            eliminar(data['id'],row);
        }
    });

});
//webservices_PAGWEB2

function Comprar() {
    //alert("id a eliminar: " + id);

    Swal.fire({
        title: 'Estas seguro?',
        text: "No podras revertir esto!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si compralo!'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: 'http://localhost/webservice_Examen3pw/carrito.php?opcion=deleteALL',
                type: 'POST',
                data: JSON.stringify({"Hola": 1}),

                success: function (result) {
                    console.log(result);
                    //alert('Se borro Correctamente');
                    Swal.fire({
                        position: 'top-end',
                        type: 'success',
                        title: 'Carrito eliminado, Regresando al catalogo',
                        showConfirmButton: false,
                        timer: 1500
                    });
                    window.open('../verTienda/ver.html','_self');
                    //setTimeout(function(){ location.reload(); }, 1502);
//      location.reload();
                },
                error: function (error) {
                    console.log(error);
                    alert('Error a chuchita le bolsearon el delete');
                }
            })
        }
    })
//    console.log(JSON.parse(jsData));

}

function editar(id) {
    //alert("id a editar: " + id);
    //localStorage.setItem('articuloId',id);
    //window.open('editar.html','_self');
}

function insertar() {
    //window.open('insertar.html','_self');
}

function sacarTotal() {
    $.ajax({
        url: 'http://localhost/webservice_Examen3pw/carrito.php?opcion=index',
        type: 'POST',
        data: JSON.stringify({"Hola": 1}),

        success: function (result) {
            console.log(result['data']);
            console.log(result['data'].length);
            Total = 0;
            for (let i = 0; i < result['data'].length ; i++) {
                Total = Number(Total) + Number( result['data'][i].total );
                console.log(result['data'][i].total);
            }
            console.log(Total);
            var table_body = '<h1 align="center">El total de la compra es ';
            table_body += Total;
            table_body += '</h1>';
                $('#ValorTotal').html(table_body);
            //alert('Se borro Correctamente');

            //setTimeout(function(){ location.reload(); }, 1502);
//      location.reload();
        },
        error: function (error) {
            console.log(error);
            alert('Error a chuchita le bolsearon el delete');
        }
    })

}

function returnCarrito(){
    window.open('../verTienda/ver.html','_self');
}

function eliminarTodo() {
        //alert("id a eliminar: " + id);
        Swal.fire({
            title: 'Estas seguro?',
            text: "No podras revertir esto!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si Eliminalo!'
        }).then((result) => {
            if (result.value) {
                console.log(QueryRepuesto);
                for (let i = 0; i < QueryRepuesto.length ; i++) {
                    console.log(QueryRepuesto[i]);
                    $.ajax({
                    url: 'http://localhost/webservice_Examen3pw/carrito.php?opcion=delete',
                    type: 'POST',
                    data: JSON.stringify(QueryRepuesto[i]),

                    success: function (result) {
                        console.log(result);
                        //alert('Se borro Correctamente');
                        Swal.fire({
                            position: 'top-end',
                            type: 'success',
                            title: 'Carrito eliminado, Regresando al catalogo',
                            showConfirmButton: false,
                            timer: 1500
                        });
                        window.open('../verTienda/ver.html','_self');
                        //setTimeout(function(){ location.reload(); }, 1502);
//      location.reload();
                    },
                    error: function (error) {
                        console.log(error);
                        alert('Error a chuchita le bolsearon el delete');
                    }
                })
                }
                }
        })
}

